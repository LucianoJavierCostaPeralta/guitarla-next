import { useRouter } from "next/router";

const useStaticData = () => {
  const { basePath } = useRouter();

  const defaultData = {
    layout: {
      header: {
        logo: `${basePath}/images/logo.svg`,
        banner: `${basePath}/images/header.jpg`,
        links: [
          {
            label: "Home",
            link: "/",
          },
          {
            label: "About",
            link: "/about",
          },
          {
            label: "Blog",
            link: "/blog",
          },
          {
            label: "Store",
            link: "/store",
          },
          {
            label: "Courses",
            link: "/courses",
          },
        ],
      },
      footer: {
        logo: `${basePath}/images/logo.svg`,
        links: [
          {
            label: "Home",
            link: "/",
          },
          {
            label: "About",
            link: "/about",
          },
          {
            label: "Blog",
            link: "/blog",
          },
          {
            label: "Store",
            link: "/store",
          },
          {
            label: "Courses",
            link: "/courses",
          },
        ],
      },
    },
    homePage: {
      title: "Home",
    },
    storePage: {
      title: "Store",
    },
    blogPage: {
      title: "Blog",
    },
    aboutPage: {
      title: "About",
    },
    coursesPage: {
      title: "Courses",
    },
  };

  return {
    defaultData,
  };
};

export default useStaticData;
