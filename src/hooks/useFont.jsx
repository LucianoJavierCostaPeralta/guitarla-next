import { Inter as InterFont } from "next/font/google";

export const inter = InterFont({
  weight: ["400", "700"],
  subsets: ["latin"],
});
