import {
  ColorSchemeProvider,
  LoadingOverlay,
  MantineProvider,
} from "@mantine/core";
import { Notifications } from "@mantine/notifications";
import { Router } from "next/router";
import React, { useEffect, useState } from "react";
import theme from "./utils/theme";

const Providers = ({ children }) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    Router.events.on("routeChangeStart", () => {
      setLoading(true);
    });

    Router.events.on("routeChangeComplete", () => {
      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
      setLoading(false);
    });

    Router.events.on("routeChangeError", () => setLoading(false));
  });
  return (
    <ColorSchemeProvider colorScheme="light">
      <MantineProvider theme={{ ...theme }} withGlobalStyles withNormalizeCSS>
        <Notifications />
        <LoadingOverlay
          style={{ position: "fixed" }}
          visible={loading}
          loaderProps={{
            size: "xl",
            color: theme.colors.primary[6],
          }}
        />
        {children}
      </MantineProvider>
    </ColorSchemeProvider>
  );
};

export default Providers;
