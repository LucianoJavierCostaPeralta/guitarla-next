export const urlPath = (url) => url.split("?")[0].split("/").reverse()[0];

export const capitalizeFirstLetter = (str) =>
  str.charAt(0).toUpperCase() + str.slice(1);

export const formateCurrency = (c) =>
  c.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });

export const formatDateTime = (dateString) => {
  const date = new Date(dateString);

  const dateOptions = { day: "2-digit", month: "2-digit", year: "numeric" };
  const timeOptions = { hour: "2-digit", minute: "2-digit", second: "2-digit" };

  const formattedDate = new Intl.DateTimeFormat("en-US", dateOptions).format(
    date
  );
  const formattedTime = new Intl.DateTimeFormat("en-US", timeOptions).format(
    date
  );

  return `${formattedDate} ${formattedTime}`;
};
