import { createStyles } from "@mantine/core";

// Define all classes using theme vars
const useGlobalStyles = createStyles((theme, params) => {
  return {};
});
const GlobalStyles = (params) => {
  const { classes: gClasses, cx: gCx } = useGlobalStyles(params);
  return { gClasses, gCx };
};

export default GlobalStyles;
