import MenuUp from "./menuUp.svg";
import MenuDown from "./menuDown.svg";

let Icons = {};

Icons = {
  menuUp: <MenuUp />,
  menuDown: <MenuDown />,
};

export default Icons;
