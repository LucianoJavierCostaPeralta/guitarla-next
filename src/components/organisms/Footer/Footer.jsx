import theme from "@/lib/utils/theme";
import useGlobalStyles from "@/styles/useGlobalStyles";
import {
  Box,
  Container,
  Flex,
  Footer,
  Grid,
  Group,
  List,
  MediaQuery,
  Text,
} from "@mantine/core";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import {
  BrandFacebook,
  BrandInstagram,
  BrandWhatsapp,
} from "tabler-icons-react";
import useStyles from "./footer.style";

const FooterComponent = ({ logo, links }) => {
  const { classes, cx } = useStyles();
  const { gClasses, gCx } = useGlobalStyles();

  const Logo = ({ src }) => (
    <Link href="/">
      <Image src={src ?? null} alt="Logo" width={200} height={50} priority />
    </Link>
  );

  const mainLinks = links?.map((item) => (
    <List.Item key={item.link}>
      <Link href={item.link ?? null} className={classes.item}>
        {item.label}
      </Link>
    </List.Item>
  ));

  const SocialMediaLinks = () => (
    <Flex
      direction="row"
      align="center"
      justify="start"
      columnGap={10}
      className={classes.socialLinks}
    >
      <Link href="#" target="_blank">
        <BrandInstagram size={30} />
      </Link>
      <Link href="#" target="_blank">
        <BrandFacebook size={30} />
      </Link>
      <Link href="#" target="_blank">
        <BrandWhatsapp size={30} />
      </Link>
    </Flex>
  );

  return (
    <Footer className={classes.footer}>
      <Container size="xxl">
        <Grid>
          <Grid.Col span={12} md={6}>
            <Flex direction="column" align="start" justify="center">
              <Logo src={logo} />
            </Flex>
          </Grid.Col>
          <Grid.Col md={6} span={12}>
            <Flex direction="column">
              <Text
                weight={700}
                color={theme.colors.primary[6]}
                size={25}
                transform="uppercase"
                mb="tiny"
              >
                Contact
              </Text>
              <Box my="tiny">
                <Link
                  href="mailto:support@guitarla.com"
                  className={classes.email}
                >
                  support@guitarla.com
                </Link>
              </Box>
              <Text
                weight={400}
                color={theme.colors.white[0]}
                size={20}
                my="tiny"
              >
                Follow us on social media
              </Text>
              <Grid my="tiny">
                <Grid.Col span={12} md={6}>
                  <SocialMediaLinks />
                  <Text
                    weight={400}
                    color={theme.colors.white[0]}
                    size={20}
                    my="tiny"
                  >
                    @{new Date().getFullYear()}&nbsp;&nbsp;Guitarla. All rights
                    reserved.
                  </Text>
                </Grid.Col>
              </Grid>
            </Flex>
          </Grid.Col>
        </Grid>
      </Container>
    </Footer>
  );
};

export default FooterComponent;
