import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {
    footer: {
      background: theme.colors.black[6],
      padding: `${theme.spacing.lg}px 0`,
    },
    list: {
      display: "flex",
      [theme.fn.smallerThan("md")]: {
        flexDirection: "column",
        li: {
          margin: `${theme.spacing.xxs}px 0`,
        },
      },
    },
    item: {
      color: theme.colors.white[0],
      fontSize: 20,
      marginRight: theme.spacing.xxs,
      fontWeight: 700,
      textDecoration: "none",
      lineHeight: "30px",
      textTransform: "uppercase",
      "&:hover": {
        color: theme.colors.primary[6],
        transition: "all 0.5s",
        background: "none",
      },
    },
    socialLinks: {
      a: {
        svg: {
          color: theme.colors.white[0],
          "&:hover": {
            color: theme.colors.primary[6],
          },
        },
      },
    },
    email: {
      color: theme.colors.white[0],
      textDecoration: "none",
      fontSize: 20,
      fontWeight: 400,
      "&:hover": {
        color: theme.colors.primary[6],
        textDecoration: "underline",
      },
    },
  };
});
const Footer = (params) => useStyles(params);

export default Footer;
