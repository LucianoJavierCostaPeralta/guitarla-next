import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {
    header: {
      background: `linear-gradient(to right, ${theme.fn.rgba(
        theme.colors.black[9],
        0.1
      )},  ${theme.fn.rgba(theme.colors.black[9], 0.1)}),
      linear-gradient(to right,  ${theme.fn.rgba(theme.colors.black[9], 0.1)},
      ${theme.fn.rgba(theme.colors.black[9], 0.1)}),`,
      padding: "5rem 0",
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
    },
    item: {
      color: theme.colors.white[0],
      fontSize: 20,
      padding: theme.spacing.xxs,
      fontWeight: 700,
      textDecoration: "none",
      lineHeight: "30px",
      textTransform: "uppercase",
      "&:hover": {
        color: theme.colors.primary[6],
        transition: "all 0.5s",
        background: "none",
      },
    },
    active: {
      color: theme.colors.primary[6],
      background: "transparent",
    },
    burger: {
      border: "none",
      cursor: "pointer",
      color: theme.colors.primary[6],
      width: theme.spacing.md,
      background: "none",
      [theme.fn.largerThan("md")]: {
        display: "none",
      },
      svg: {
        width: theme.spacing.md,
        path: {
          fill: theme.colors.primary[6],
        },
      },
    },
  };
});
const Header = (params) => useStyles(params);

export default Header;
