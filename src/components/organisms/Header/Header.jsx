import theme from "@/lib/utils/theme";
import Icons from "@/styles/icons";
import {
  Container,
  Flex,
  Grid,
  Group,
  Header,
  List,
  MediaQuery,
  Menu,
  useMantineTheme,
} from "@mantine/core";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import useStyles from "./header.style";

function HeaderComponent({ logo, links, banner }) {
  const [opened, setOpened] = useState(false);
  const { classes, cx } = useStyles();
  const router = useRouter();

  useEffect(() => {
    setOpened(false);
  }, [router.pathname]);

  const Logo = ({ src }) => (
    <Link href="/">
      <Image src={src ?? null} alt="Logo" width={200} height={50} priority />
    </Link>
  );

  const mainLinks = links.map((item, index) => (
    <List.Item key={index}>
      <Menu.Item
        component={Link}
        href={item.link ?? null}
        className={cx(classes.item, {
          [classes.active]: item.link === router.pathname,
        })}
      >
        {item.label}
      </Menu.Item>
    </List.Item>
  ));

  const DekstopMenu = () => (
    <Flex justify="end" align="center">
      <nav role="navigation" aria-label="Primary menu">
        <List
          listStyleType="none"
          sx={{
            display: "flex",
          }}
        >
          {mainLinks}
        </List>
      </nav>
    </Flex>
  );

  const MobileMenu = () => (
    <nav role="navigation" aria-label="Primary menu">
      <List listStyleType="none">{mainLinks}</List>
    </nav>
  );

  return (
    <Header
      className={classes.header}
      sx={{
        background: `url(${banner})`,
      }}
    >
      <Container size="xxl">
        <Menu offset={0} width="100%" radius={0}>
          <Grid align="center">
            <Grid.Col span={4}>
              <Logo src={logo} />
            </Grid.Col>
            <MediaQuery smallerThan="md" styles={{ display: "none" }}>
              <Grid.Col span={8}>
                <DekstopMenu />
              </Grid.Col>
            </MediaQuery>

            <MediaQuery largerThan="md" styles={{ display: "none" }}>
              <Grid.Col span={8}>
                <Group position="right" align="center">
                  <Menu.Target>
                    <button
                      onClick={() => setOpened(!opened)}
                      className={classes.burger}
                    >
                      {opened ? Icons.menuUp : Icons.menuDown}
                    </button>
                  </Menu.Target>
                </Group>
              </Grid.Col>
            </MediaQuery>
          </Grid>

          <MediaQuery largerThan="md" styles={{ display: "none" }}>
            <Menu.Dropdown
              p={0}
              mt="xxs"
              sx={{
                background: theme.colors.black[8],
                border: 0,
              }}
            >
              <MobileMenu />
            </Menu.Dropdown>
          </MediaQuery>
        </Menu>
      </Container>
    </Header>
  );
}

export default HeaderComponent;
