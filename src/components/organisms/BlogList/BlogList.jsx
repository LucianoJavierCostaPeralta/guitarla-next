import PostCard from "@/components/molecules/PostCard";
import { Grid } from "@mantine/core";
import React from "react";

const BlogList = ({ posts }) => {
  return (
    <>
      {posts?.length && (
        <Grid gutter={20} my={16}>
          {posts?.map((item) => (
            <PostCard key={item.id} post={item.attributes} />
          ))}
        </Grid>
      )}
    </>
  );
};

export default BlogList;
