import { Container } from "@mantine/core";
import { TitlePage } from "../atoms/TitlePage";

export const CoursesPage = ({ title }) => {
  return (
    <Container size="xxl">
      <TitlePage title={title} />
    </Container>
  );
};
