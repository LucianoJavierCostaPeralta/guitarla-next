import { Container } from "@mantine/core";
import BlogList from "../organisms/BlogList/BlogList";
import { TitlePage } from "../atoms/TitlePage";

export const BlogPage = ({ posts, title }) => {
  return (
    <Container size="xxl">
      <TitlePage title={title} />
      <BlogList posts={posts} />
    </Container>
  );
};
