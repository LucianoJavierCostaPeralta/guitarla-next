import { Container } from "@mantine/core";
import React from "react";

import { TitlePage } from "../atoms/TitlePage";

export const AboutPage = ({ title }) => {
  return (
    <Container size="xxl">
      <TitlePage title={title} />
    </Container>
  );
};
