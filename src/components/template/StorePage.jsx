import { Container } from "@mantine/core";
import React from "react";
import { TitlePage } from "../atoms/TitlePage";
import GuitarList from "../molecules/GuitarList/GuitarList";

const StorePage = ({ guitars, title }) => {
  return (
    <Container size="xxl">
      <GuitarList guitar={guitars} />
    </Container>
  );
};

export default StorePage;
