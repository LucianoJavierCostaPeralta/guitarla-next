import { Container } from "@mantine/core";
import React from "react";
import { TitlePage } from "../atoms/TitlePage";

const HomePage = ({ title }) => {
  return (
    <Container size="xxl">
      <TitlePage title={title} />
    </Container>
  );
};

export default HomePage;
