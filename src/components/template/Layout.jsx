import FooterComponent from "../organisms/Footer/Footer";
import HeaderComponent from "../organisms/Header/Header";

const Layout = ({ header, footer, children }) => {
  return (
    <>
      <HeaderComponent {...header} />
      <main style={{ margin: "1rem 0" }}>{children}</main>
      <FooterComponent {...footer} />
    </>
  );
};

export default Layout;
