import theme from "@/lib/utils/theme";
import { Title } from "@mantine/core";

export const TitlePage = ({ title }) => {
  return (
    <>
      {title && (
        <Title
          size="h1"
          color={theme.colors.primary[6]}
          weight={700}
          transform="capitalize"
        >
          {title}
        </Title>
      )}
    </>
  );
};
