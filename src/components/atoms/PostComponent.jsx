import { Container } from "@mantine/core";
import RichText from "../molecules/RichText/RichText";
import Image from "next/image";

const PostComponent = ({ post }) => {
  const srcImage = post?.image.data.attributes.formats.medium.url;
  return (
    <Container size="xxl">
      <Image
        src={srcImage}
        width={300}
        height={300}
        alt={`Imagen del post ${post?.name}`}
        style={{
          width: "100%",
          height: "100%",
          objectFit: "cover",
        }}
      />
      <RichText content={post?.content} />
    </Container>
  );
};

export default PostComponent;
