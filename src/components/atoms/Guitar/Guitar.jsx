import theme from "@/lib/utils/theme";
import { Flex, Group, Stack, Text, Title } from "@mantine/core";
import Image from "next/image";
import Link from "next/link";
import React from "react";

import useStyles from "./guitar.styles";
import { formateCurrency } from "@/lib/utils/utils";

const Guitar = ({ image, name, content, price, url }) => {
  const { classes } = useStyles();

  const srcImage = image.data.attributes.formats.medium.url;

  return (
    <Flex direction="column" rowGap={20}>
      {name && (
        <Title size={32} align="start" weight={700} mb="tiny">
          {name}
        </Title>
      )}

      {image && (
        <div>
          <Image
            src={srcImage}
            width={250}
            height={500}
            alt={`Guitar image ${name}`}
            priority
          />
        </div>
      )}

      {price && (
        <Text weight={700} size={50} color={theme.colors.primary[6]}>
          {formateCurrency(price)}
        </Text>
      )}

      {url && (
        <Link className={classes.link} href={`/guitar/${url ?? null}`}>
          See More
        </Link>
      )}
    </Flex>
  );
};

export default Guitar;
