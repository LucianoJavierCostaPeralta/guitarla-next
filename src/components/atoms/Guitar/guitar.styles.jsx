import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {
    link: {
      textDecoration: "none",
      textTransform: "uppercase",
      fontWeight: 400,
      color: theme.colors.white[0],
      fontSize: 24,
      background: theme.colors.black[9],
      borderRadius: "5px",
      padding: 16,
      lineHeight: 1,
      textAlign: "center",
      "&:hover": {
        background: theme.colors.primary[6],
        color: theme.colors.black[9],
      },
    },
  };
});
const GuitarStyles = (params) => useStyles(params);

export default GuitarStyles;
