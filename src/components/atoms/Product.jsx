import theme from "@/lib/utils/theme";
import { formateCurrency } from "@/lib/utils/utils";
import {
  Container,
  Flex,
  Group,
  Stack,
  Text,
  Title,
  useMantineTheme,
} from "@mantine/core";
import Image from "next/image";
import React from "react";

const Product = ({ image, name, content, price }) => {
  const srcImage = image.data.attributes.formats.medium.url;
  const themeMantine = useMantineTheme();

  return (
    <Container size="xxl" my="md">
      <Flex direction="column" justify="start">
        <Flex
          sx={{
            [themeMantine.fn.smallerThan("md")]: {
              flexDirection: "column",
            },
            alignItems: "stretch",
          }}
        >
          {image && (
            <div>
              <Image
                src={srcImage}
                width={250}
                height={500}
                alt={`Product image ${name}`}
                priority
              />
            </div>
          )}

          <Group spacing={0} position="apart" align="stretch">
            <Stack spacing="md" align="start">
              <div>
                {name && (
                  <Title size={50} align="start" weight={700} mb="tiny">
                    {name}
                  </Title>
                )}

                {content && (
                  <Text weight={400} size={24}>
                    {content}
                  </Text>
                )}
              </div>
              {price && (
                <Text weight={700} size={60} color={theme.colors.primary[6]}>
                  {formateCurrency(price)}
                </Text>
              )}
            </Stack>
          </Group>
        </Flex>
      </Flex>
    </Container>
  );
};

export default Product;
