import { Flex, Grid, Stack, Text, Title } from "@mantine/core";
import Image from "next/image";
import Link from "next/link";
import useStyles from "./postCard.style";
import { formatDateTime } from "@/lib/utils/utils";

const PostCard = ({ post }) => {
  const { classes } = useStyles();
  console.log(post);

  const srcImage = post?.image.data.attributes.formats.medium.url;

  return (
    <Grid.Col md={12} lg={6} xl={4}>
      <Link href={`/blog/${post?.url}`} className={classes.link}>
        <Flex direction="column">
          <Image
            src={srcImage}
            width={300}
            height={300}
            alt={`Image del post ${post?.name}`}
            className={classes.image}
          />
          <Stack
            p={10}
            spacing={5}
            justify="space-between"
            sx={{
              height: "100%",
            }}
          >
            <Title className={classes.title}>{post?.name}</Title>
            <Text size={22} mt={4}>
              <strong>Publicado: </strong>
              {formatDateTime(post?.publishedAt)}
            </Text>
          </Stack>
        </Flex>
      </Link>
    </Grid.Col>
  );
};

export default PostCard;
