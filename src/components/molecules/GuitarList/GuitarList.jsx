import Guitar from "@/components/atoms/Guitar/Guitar";
import { Grid } from "@mantine/core";

const GuitarList = ({ guitar }) => {
  return (
    <>
      {guitar && (
        <Grid gutter={20}>
          {guitar.map((item) => {
            return (
              <Grid.Col key={item.id} span={12} md={6} lg={4} xl={3} my="md">
                <Guitar {...item.attributes} />
              </Grid.Col>
            );
          })}
        </Grid>
      )}
    </>
  );
};

export default GuitarList;
