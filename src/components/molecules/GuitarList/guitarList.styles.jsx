import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {};
});
const GuitarListStyles = (params) => useStyles(params);

export default GuitarListStyles;
