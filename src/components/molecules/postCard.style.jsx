import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {
    link: {
      textDecoration: "none",
      color: theme.colors.black[12],
      background: theme.colors.primary[6],
      borderRadius: "0 0 40px 0",
      display: "flex",
      height: "100%",
      boxShadow: "0 4px 4px 0 rgba(0,0,0,0.15)",
      "&:hover": {
        img: {
          opacity: 0.8,
        },
        h1: {
          textDecoration: "underline",
        },
        background: theme.colors.black[12],
        color: theme.colors.primary[6],
      },
    },
    image: {
      width: "100%",
      height: "100%",
      objectFit: "cover",
      borderRadius: 5,
    },
  };
});
const PostCardStyles = (params) => useStyles(params);

export default PostCardStyles;
