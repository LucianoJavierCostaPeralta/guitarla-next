import { Text } from "@mantine/core";
import useStyles from "./richText.styles";

const RichText = ({ content }) => {
  const { classes } = useStyles();

  return (
    <Text
      className={classes.content}
      dangerouslySetInnerHTML={{ __html: content }}
    />
  );
};

export default RichText;
