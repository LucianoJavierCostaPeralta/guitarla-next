import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme, params) => {
  return {
    content: {
      color: theme.colors.black[12],
      fontSize: theme.fontSizes.h5[0],
      letterSpacing: "0.12px",
      lineHeight: "30px",
      ol: {
        marginBottom: theme.spacing.xs,
        li: {
          paddingLeft: theme.spacing.xxxs,
        },
      },
      ul: {
        marginBottom: theme.spacing.xs,
        li: {
          paddingLeft: theme.spacing.xxxs,
        },
      },
      h1: {
        fontWeight: 700,
        fontSize: theme.fontSizes.h1[0],
        lineHeight: "60px",
        color: theme.colors.primary[6],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h1[1],
          lineHeight: "45px",
        },
      },
      h2: {
        fontFamily: theme.headings.fontFamily,
        fontWeight: 700,
        fontSize: theme.fontSizes.h2[0],
        lineHeight: "53px",
        color: theme.colors.black[12],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h2[1],
          lineHeight: "35px",
        },
      },
      h3: {
        fontFamily: theme.headings.fontFamily,
        fontWeight: 700,
        fontSize: theme.fontSizes.h3[0],
        lineHeight: "45px",
        color: theme.colors.black[12],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h3[1],
          lineHeight: "25px",
        },
      },
      h4: {
        fontFamily: theme.headings.fontFamily,
        fontWeight: 700,
        fontSize: theme.fontSizes.h4[0],
        lineHeight: "35px",
        color: theme.colors.black[12],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h4[1],
          lineHeight: "23px",
        },
      },
      h5: {
        fontFamily: theme.headings.fontFamily,
        fontWeight: 700,
        fontSize: theme.fontSizes.h5[0],
        lineHeight: "32px",
        color: theme.colors.black[12],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h5[1],
          lineHeight: "20px",
        },
      },
      h6: {
        fontFamily: theme.headings.fontFamily,
        fontWeight: 700,
        fontSize: theme.fontSizes.h6[0],
        lineHeight: "30px",
        color: theme.colors.black[12],
        [theme.fn.smallerThan("sm")]: {
          fontSize: theme.fontSizes.h6[1],
          lineHeight: "18px",
        },
      },
      strong: {
        fontWeight: "bold",
        color: theme.colors.dark[9],
        fontSize: theme.fontSizes.body[0],
      },
      b: {
        fontWeight: "bold",
        color: theme.colors.dark[9],
        fontSize: theme.fontSizes.body[0],
      },
      em: {
        fontWeight: 600,
      },
    },
  };
});
const RichTextStyles = (params) => useStyles(params);

export default RichTextStyles;
