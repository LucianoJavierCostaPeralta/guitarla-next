import { BlogPage } from "@/components/template/BlogPage";
import useStaticData from "@/hooks/useStaticData";
import React from "react";

const Blog = () => {
  const { defaultData } = useStaticData();

  return <BlogPage {...defaultData.blogPage} />;
};

export default Blog;
