import { CoursesPage } from "@/components/template/CoursesPage";
import useStaticData from "@/hooks/useStaticData";
import React from "react";

const Courses = () => {
  const { defaultData } = useStaticData();

  return <CoursesPage {...defaultData.coursesPage} />;
};

export default Courses;
