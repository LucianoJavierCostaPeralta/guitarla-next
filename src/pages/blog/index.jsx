import { BlogPage } from "@/components/template/BlogPage";
import useStaticData from "@/hooks/useStaticData";
import React from "react";

const Blog = ({ posts }) => {
  const { defaultData } = useStaticData();

  return <BlogPage {...defaultData.blogPage} posts={posts} />;
};

export default Blog;

export const getStaticProps = async () => {
  const baseUrl = `${process.env.API_URL}/posts?populate=image`;
  const res = await fetch(baseUrl);
  const { data } = await res.json();

  return {
    props: {
      posts: data,
    },
  };
};
