import PostComponent from "@/components/atoms/PostComponent";

const Post = ({ post }) => {
  const postI = post[0].attributes;
  return <PostComponent post={postI} />;
};

export default Post;
export const getStaticPaths = async () => {
  const baseUrl = `${process.env.API_URL}/posts`;
  const res = await fetch(baseUrl);
  const { data } = await res.json();

  const paths = data.map((item) => ({
    params: {
      url: item.attributes.url,
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }) => {
  const baseUrl = `${process.env.API_URL}/posts`;
  const res = await fetch(
    `${baseUrl}?filters[url]=${params.url}&populate=image`
  );
  const { data } = await res.json();

  return {
    props: {
      post: data,
      revalidate: 10,
    },
  };
};
