import StorePage from "@/components/template/StorePage";
import useStaticData from "@/hooks/useStaticData";

const Store = ({ guitars }) => {
  const { defaultData } = useStaticData();

  return <StorePage guitars={guitars} {...defaultData.storePage} />;
};

export default Store;

export const getStaticProps = async () => {
  const baseUrl = `${process.env.API_URL}/guitars?populate=image`;
  const res = await fetch(baseUrl);
  const { data: guitars } = await res.json();

  return {
    props: {
      guitars,
      revalidate: 10,
    },
  };
};
