import Product from "@/components/atoms/Product";

const ProductPage = ({ guitar }) => {
  const product = guitar[0]?.attributes;

  return <Product {...product} />;
};

export default ProductPage;

export const getStaticPaths = async () => {
  const baseUrl = `${process.env.API_URL}/guitars`;
  const res = await fetch(baseUrl);
  const { data } = await res.json();

  const paths = data.map((item) => ({
    params: {
      url: item.attributes.url,
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }) => {
  const baseUrl = `${process.env.API_URL}/guitars`;
  const res = await fetch(
    `${baseUrl}?filters[url]=${params.url}&populate=image`
  );
  const { data } = await res.json();

  return {
    props: {
      guitar: data,
      revalidate: 10,
    },
  };
};
