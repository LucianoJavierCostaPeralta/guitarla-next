import { AboutPage } from "@/components/template/AboutPage";
import useStaticData from "@/hooks/useStaticData";
import React from "react";

const About = () => {
  const { defaultData } = useStaticData();

  return <AboutPage {...defaultData.aboutPage} />;
};

export default About;
