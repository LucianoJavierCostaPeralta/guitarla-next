import Layout from "@/components/template/Layout";
import useStaticData from "@/hooks/useStaticData";
import Providers from "@/lib/Providers";
import { capitalizeFirstLetter, urlPath } from "@/lib/utils/utils";
import Head from "next/head";
import { useRouter } from "next/router";

export default function App(props) {
  const { Component, pageProps } = props;
  const { defaultData } = useStaticData();
  const router = useRouter();
  const { url } = router.query;
  const baseUrl = capitalizeFirstLetter(urlPath(router.pathname));

  const titlePage = url
    ? capitalizeFirstLetter(url)
    : router.pathname === "/"
    ? "Home"
    : baseUrl;

  return (
    <>
      <Head>
        <title>{`GuitarLA - ${titlePage}`}</title>
      </Head>

      <Providers>
        <Layout {...defaultData.layout}>
          <Component {...pageProps} />
        </Layout>
      </Providers>
    </>
  );
}
